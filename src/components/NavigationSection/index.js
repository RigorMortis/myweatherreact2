import React, {useEffect, useState} from 'react'
import {NavigationContainer,NavigationWrapper, NavigationRow, Column1,TextWrapper, TopLine, Heading, Subtitle} from "./NavigationElements";
import {Form, FormButton, FormContent, FormInput, FormLabel, FormWrap} from "../NavigationSection/NavigationElements";
import locationService from "../services/locations";


const Navigation = ({lightBg, id, topLine, lightText, headline, darkText,description, imgStart}) => {
    const [locations, setLocations] = useState([])
    const [newUsername, setNewUsername ] = useState('')
    const [newLocation1, setNewLocation1 ] = useState('')
    const [newLocation2, setNewLocation2] = useState('')
    const [message, setMessage] = useState(null) //success

    //Haetaan data palvelimelta
    useEffect(() => {
        locationService
            .getAll()
            .then(initialLocations => {
                setLocations(initialLocations)
            })
    }, [])

    //Lisää henkilö (+ ehdot onnistuneeseen lisäykseen)
    const addLocation = (event) =>{
        event.preventDefault()
        const locationObject = {username: newUsername, location1: newLocation1, location2: newLocation2}
        if(locationObject.username == "" || locationObject.location1 == "" || locationObject.location2 == ""){
            setMessage(`Täytä puuttuvat kentät!`)
            setTimeout(() => {
                setMessage(null)
            }, 3000)
        }else{
            locationService
                .create(locationObject)
                .then(returnedLocation => {
                    setMessage(`"${locationObject.location1} ja ${locationObject.location2}" Tiedot lähetetty!`)
                    setTimeout(() => {
                        setMessage(null)
                    }, 3000)
                    setLocations(locations.concat(returnedLocation))
                    setNewUsername('')
                    setNewLocation1('')
                    setNewLocation2('')
                })
                .catch((error) =>
                    // pääset käsiksi palvelimen palauttamaan virheilmoitusolioon näin
                    setMessage(error.response.data.error)
                )
            setTimeout(() => {
                setMessage(null)
            }, 4000)
        }
    }

    //Synkronoi syötekenttiin tehdyt muutokset

    const handleUsernameChange = (event) =>{
        setNewUsername(event.target.value)
    }

    const handleL1Change = (event) =>{
        setNewLocation1(event.target.value)
    }

    const handleL2Change = (event) =>{
        setNewLocation2(event.target.value)
    }

    return(
        <>
            <NavigationContainer lightBg={lightBg} id={id}>
                <NavigationWrapper>
                    <NavigationRow imgStart={imgStart}>
                        <Column1>
                            <TextWrapper>
                                <TopLine>{topLine}</TopLine>
                                <Heading lightText={lightText}>{headline}</Heading>
                                <Subtitle darkText={darkText}>{description}</Subtitle>
                            </TextWrapper>
                        </Column1>
                            <FormWrap>
                                    <FormContent>
                                        <Form onSubmit={addLocation}>
                                            <TopLine>Save your location info</TopLine>
                                            <FormLabel htmlFor='for'>Username</FormLabel>
                                            <FormInput type='text' username={newUsername} onChange={handleUsernameChange} required/>
                                            <FormLabel htmlFor='for'>From</FormLabel>
                                            <FormInput type='text' location1={newLocation1} onChange={handleL1Change} required/>
                                            <FormLabel htmlFor='to'>To</FormLabel>
                                            <FormInput type='text' location2={newLocation2} onChange={handleL2Change} required/>
                                            <FormButton type='submit'>Save</FormButton>
                                        </Form>
                                    </FormContent>
                            </FormWrap>
                    </NavigationRow>

                </NavigationWrapper>
            </NavigationContainer>
        </>
    )
}

export default Navigation