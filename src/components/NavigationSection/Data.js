export const navigationobjOne = {
    id: 'navigate',
    lightBg: false,
    lightTextDesc: true,
    topLine: 'Navigation',
    headline: 'Navigate from one place to another',
    description: 'Use the map below to navigate and save your location for later use.',
}