import styled from 'styled-components'

export const NavigationContainer = styled.div`
  color: #fff;
  background: ${({lightBg}) => (lightBg ? '#25c790' : '#25c790')};
  
  @media screen and (max-width: 768px){
    padding: 100px 0 0;
  }
`
export const NavigationWrapper = styled.div`
  display: grid;
  z-index: 1;
  height: 100%;
  max-width: 1100px;
  margin-right: auto;
  margin-left: auto;
  padding: 0 24px 0;
  justify-content: center;
`
export const Column1=styled.div`
  margin-bottom: 15px;
  padding: 0 15px;
  grid-area: col1;
`

export const Column2=styled.div`
  margin-bottom: 15px;
  padding: 0 15px;
  grid-area: col2;
`

export const TextWrapper=styled.div`
  max-width: 540px;
  padding-top: 50px;
  padding-bottom: 0;
`

export const TopLine = styled.p`
  color: black;
  font-size: 16px;
  line-height: 16px;
  font-weight: 700;
  letter-spacing: 1.4px;
  text-transform: uppercase;
  margin-bottom: 16px;
`
export const Heading = styled.h1`
  margin-bottom: 24px;
  font-size: 48px;
  line-height: 1.1;
  font-weight: 600;
  color: ${({lightText}) => (lightText ? 'black' : '#fff')};
  text-shadow: 0 1px 3px rgb(0,0,0,0.9);
  
  @media screen and (max-width: 480px){
    font-size: 32px;
  }
`

export const Subtitle = styled.p`
  max-width: 440px;
  margin-bottom: 35px;
  font-size: 18px;
  line-height: 24px;
  color: ${(darkText) => (darkText ? '#010606' : "#fff")};
`

export const NavigationRow = styled.div`
  display: grid;
  grid-auto-columns: minmax(auto, 1fr);
  align-items: center;
  grid-template-areas: ${({imgStart}) => (imgStart ? `'col2 col1'` : `'col1 col2'`)};
  
  @media screen and (max-width: 768px){
    grid-template-areas: ${({imgStart}) => (imgStart ? `'col1' 'col2'`: `'col1' 'col2'`)};
  }
`
export const FormWrap = styled.div`
  display: inline;
  z-index: 1;
  height: 100%;
  max-width: 100%;
  margin-right: auto;
  margin-left: 0;
  padding: 100px 24px 100px;

  @media screen and (max-width: 400px){
    height: 100%;
  }
`;

export const FormContent = styled.div`
  height: 50%;
  display: inline;
  flex-direction: column;
  justify-content: center;

  @media screen and (max-width: 400px){
    padding: 10px;
  }
`;

export const Form = styled.form`
  background: #25c790;
  max-width: 400px;
  height: auto;
  width: 300%;
  z-index: 1;
  display: inline-grid;
  margin-left: 80px;
  padding: 25px 32px;
  border-radius: 4px;
  
  @media screen and (max-width: 400px){
    padding: 32px 32px 0;
  }
`;

export const  FormLabel = styled.label`
  margin-bottom: 8px;
  font-size: 14px;
  color: black;
`;

export const FormInput = styled.input`
  padding: 16px 16px;
  margin-bottom: 30px;
  border: none;
  border-radius: 4px;
`;

export const FormButton = styled.button`
  background: black;
  padding: 16px 0;
  border: none;
  border-radius: 4px;
  color: #fff;
  font-size: 20px;
  cursor: pointer;
`;

export const Text = styled.span`
  text-align: center;
  margin-top: 24px;
  color: #fff;
  font-size: 14px;
`;