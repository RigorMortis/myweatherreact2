import styled from 'styled-components';

export const TableContainer = styled.div`
  background:white; 
  display:grid;
  padding: 50px;
  
  @media screen and (max-width: 400px){
    padding: 100px 0;
  }
`;

export const TopLine = styled.p`
  color: black;
  font-size: 16px;
  line-height: 16px;
  font-weight: 700;
  letter-spacing: 1.4px;
  text-transform: uppercase;
  margin-top: 15px;
`;
