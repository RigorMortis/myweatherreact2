import React, {useEffect, useState} from 'react';
import locationService from '../services/locations';
import {TableContainer, TopLine} from "../LocationSection/LocationElements";
import './Location.css';

const Location = () => {

    const [locations, setLocations] = useState([])
    const [filtered, setFiltered ] = useState('')

    //filter locations
    const locationsToShow = locations.filter(location =>
        location.username.toLowerCase().includes(filtered.toLowerCase()))

    //Haetaan data palvelimelta
    useEffect(() => {
        locationService
            .getAll()
            .then(initialLocations => {
                setLocations(initialLocations)
            })
    }, []);

    const handleFilterChange = (event) => {
        setFiltered(event.target.value)
    }

    return(
        <>
            <TableContainer id='saved'>
                <TopLine>SAVED LOCATIONS</TopLine>
                <form>
                    <input
                    value={filtered}
                    onChange={handleFilterChange}
                    />
                </form>
                <table className="styled-table">
                    <thead>
                    <tr>
                        <th>Username</th>
                        <th>From</th>
                        <th>To</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>{locationsToShow.map((location, i) =>
                            <p key={i}>{`${location.username}`}</p>
                        )}
                        </td>
                        <td>{locationsToShow.map((location, i) =>
                            <p key={i}>{`${location.location1}`}</p>
                        )}
                        </td>
                        <td>
                            {locationsToShow.map((location, i) =>
                                <p key={i}>{`${location.location2}`}</p>
                            )}
                        </td>
                    </tr>
                    </tbody>
                </table>
            </TableContainer>
        </>
    )
}


export default Location
