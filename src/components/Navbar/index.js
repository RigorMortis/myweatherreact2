import {FaBars} from 'react-icons/fa'
import {Nav, NavbarContainer, NavLogo, MobileIcon,NavMenu, NavItem, NavLinks, NavBtn, NavBtnLink} from "./NavbarElements";
import React from "react";
import {animateScroll as scroll} from 'react-scroll';

//Yläpalkki
const Navbar= ({toggle}) => {
    const toggleHome = () => {
        scroll.scrollToTop();
    }
    return(
        <>
            <Nav>
              <NavbarContainer>
                  <NavLogo to='/' onClick={toggleHome}>MYCITY!</NavLogo>
                  <MobileIcon onClick={toggle}>
                      <FaBars />
                  </MobileIcon>
                  <NavMenu>
                      <NavItem>
                          <NavLinks
                              to="about"
                              smooth={true}
                              duration={500}
                              spy={true}
                              exact="true"
                              offset={-80}
                          >About</NavLinks>
                      </NavItem>
                      <NavItem>
                          <NavLinks
                              to="signupSection"
                              smooth={true}
                              duration={500}
                              spy={true}
                              exact="true"
                              offset={-80}
                          >Sign Up</NavLinks>
                      </NavItem>
                      <NavItem>
                          <NavLinks
                              to="weather"
                              smooth={true}
                              duration={500}
                              spy={true}
                              exact="true"
                              offset={-80}
                          >Weather</NavLinks>
                      </NavItem>
                      <NavItem>
                          <NavLinks
                              to="navigate"
                              duration={500}
                              spy={true}
                              exact="true"
                              offset={-80}
                          >Navigate</NavLinks>
                      </NavItem>
                  </NavMenu>
                  <NavBtn>
                      <NavBtnLink to="signin">Sign In</NavBtnLink>
                  </NavBtn>
              </NavbarContainer>
            </Nav>
        </>
    )
}
export default Navbar