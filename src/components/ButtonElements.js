import styled from 'styled-components'
import {Link} from 'react-scroll'

export const Button = styled(Link)`
  border-radius: 50px;
  display: flex;
  white-space: nowrap;
  outline: none;
  border: none;
  justify-content: center;
  align-items: center;
  transition: all 0.2s ease-in-out;
  background: ${({primary}) => (primary ? '#25c790' : '#010606')};
  color: ${({dark}) => (dark ? '#010606' : '#fff')};
  font-size: ${({fontBig}) => (fontBig ? '20px' : '16px')};
  padding: ${({big}) => (big ? '14px 48px' : '12px 30px')};
  
  &:hover{
    transition: all 0.2s ease-in-out;
      background: ${({primary}) => (primary ? '#010606' : '#010606')};
      color: ${({dark}) => (dark ? '#010606' : '#fff')};
    }
`

export const PopupButton = styled.button`
  border-radius: 50px;
  display: flex;
  white-space: nowrap;
  outline: none;
  border: none;
  justify-content: center;
  align-items: center;
  transition: all 0.2s ease-in-out;
  background: ${({primary}) => (primary ? '#25c790' : '#010606')};
  color: ${({dark}) => (dark ? '#010606' : '#fff')};
  font-size: ${({fontBig}) => (fontBig ? '20px' : '16px')};
  padding: ${({big}) => (big ? '14px 48px' : '12px 30px')};

  &:hover{
    transition: all 0.2s ease-in-out;
    background: ${({primary}) => (primary ? '#010606' : '#010606')};
    color: ${({dark}) => (dark ? '#010606' : '#fff')};
  }
`