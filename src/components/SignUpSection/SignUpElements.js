import styled from 'styled-components';

export const Container = styled.div`
  background: #ffffff;
  display:grid;
  padding: 100px 0 100px 0;

  @media screen and (max-width: 768px){
    padding: 100px 0;
  }
`;

export const FormWrap = styled.div`
  border-collapse: collapse;
  margin: 25px 0;
  font-size: 0.9em;
  font-family: sans-serif;
  min-width: 400px;
  
  @media screen and (max-width: 400px){
    height: 80%;
  }
`;

export const FormContent = styled.div`
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;

  @media screen and (max-width: 400px){
    padding: 10px;
  }
`;

export const Form = styled.form`
  background: black;
  max-width: 400px;
  height: auto;
  width: 100%;
  z-index: 1;
  display: grid;
  margin: auto;
  padding: 40px 16px;
  border-radius: 4px;
  box-shadow: 0 1px 3px rgb(0,0,0,0.9);
  
  @media screen and (max-width: 400px){
    padding: 32px 32px;
  }
`;

export const FormH1 = styled.h1`
  margin-bottom: 40px;
  color: white;
  font-family: sans-serif;
  font-size: 20px;
  font-weight: 400;
  text-align: center;
`;

export const  FormLabel = styled.label`
  margin-bottom: 8px;
  margin-left: 32px;
  font-size: 14px;
  color: white;
`;

export const FormInput = styled.input`
  padding: 16px 16px;
  margin:0 32px 32px 32px;
  border: none;
  border-radius: 4px;
  box-shadow: 0 1px 3px rgb(0,0,0,0.9);
`;

export const FormButton = styled.button`
  background: #009879;
  padding: 16px 0;
  margin:0 32px 0 32px;
  border: none;
  border-radius: 4px;
  color: #fff;
  font-size: 20px;
  cursor: pointer;
`;