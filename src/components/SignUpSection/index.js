import React from 'react';
import {Container, FormWrap, FormContent, Form, FormH1, FormLabel, FormInput, FormButton} from "../SignUpSection/SignUpElements";

const SignUp = () => {
    return (
        <>
            <Container id='signup'>
                    <FormWrap>
                        <FormContent>
                            <Form action="#">
                                <FormH1>Sign up</FormH1>
                                <FormLabel htmlFor='for'>Username</FormLabel>
                                <FormInput type='username' required />
                                <FormLabel htmlFor='for'>Email</FormLabel>
                                <FormInput type='email' required />
                                <FormLabel htmlFor='for'>Password</FormLabel>
                                <FormInput type='password' required />
                                <FormButton type='submit'>Confirm</FormButton>
                            </Form>
                        </FormContent>
                    </FormWrap>
            </Container>
        </>
    )
}

export default SignUp