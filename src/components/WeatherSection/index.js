import React from 'react'
import {WeatherContainer,WeatherWrapper, WeatherWrap, WeatherRow, Column1, TextWrapper, TopLine, Heading, Subtitle} from "./WeatherElements";

const Weather = ({lightBg, id, topLine, lightText, headline, darkText,description, imgStart}) => {
    return(
        <>
            <WeatherContainer lightBg={lightBg} id={id}>
                <WeatherWrapper>
                    <WeatherRow imgStart={imgStart}>
                        <Column1>
                            <TextWrapper>
                                <TopLine>{topLine}</TopLine>
                                <Heading lightText={lightText}>{headline}</Heading>
                                <Subtitle darkText={darkText}>{description}</Subtitle>
                            </TextWrapper>
                        </Column1>
                    </WeatherRow>
                    <WeatherWrap>
                        <div className="weatherWidget"></div>
                    </WeatherWrap>
                </WeatherWrapper>
            </WeatherContainer>
        </>
    )
}

export default Weather