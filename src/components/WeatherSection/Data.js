export const weatherobjOne = {
    id: 'weather',
    lightBg: false,
    lightTextDesc: true,
    topLine: 'The weather',
    headline: 'Your local weather is here',
    description: 'Check out your weather wherever you are. Navigate and save your location.',
}