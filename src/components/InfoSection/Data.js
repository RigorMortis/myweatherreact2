import landscape from '../../images/landscape.png'
import signup from '../../images/signup.svg'
//Data-välilehdille: Voidaan valita jokaiselle välilehdelle omat infoElement-tyylit.
export const homeobjOne = {
    id: 'about',
    lightBg: false,
    lightTextDesc: true,
    topLine: 'About us',
    headline: 'City in your pocket',
    description: 'MyCity! Providing your local weekly weather and route planner. Sign in and save your location for later use.',
    buttonLabel: 'Get Started',
    buttonGoesTo: 'signupSection',
    imgStart: false,
    img: landscape,
    alt: 'Landscape',
    dark: false,
    primary: true,
    darkText: true
}

export const homeobjTwo = {
    id: 'signupSection',
    lightBg: false,
    LightText: true,
    lightTextDesc: true,
    topLine: 'Sign up',
    headline: 'Login to your account at any time',
    description: 'We have you covered! Login to save your location for later route planning.',
    buttonLabel: 'Sign up',
    buttonGoesTo: 'signup',
    imgStart: false,
    img: signup,
    alt: 'Sign Up',
    dark: false,
    primary: true,
    darkText: true
}

