import React, {useState} from 'react'
import Navbar from '../components/Navbar';
import Sidebar from "../components/Sidebar";
import HomeSection from "../components/HomeSection";
import InfoSection from "../components/InfoSection";
import WeatherSection from "../components/WeatherSection";
import NavigationSection from "../components/NavigationSection";
import {navigationobjOne} from "../components/NavigationSection/Data";
import {homeobjOne} from "../components/InfoSection/Data";
import {homeobjTwo} from "../components/InfoSection/Data";
import {weatherobjOne} from "../components/WeatherSection/Data";
import SignIn from "../components/SignInSection";
import SignUp from "../components/SignUpSection";
import Location from "../components/LocationSection";

const Home = () => {
    const [isOpen, setIsOpen] = useState(false);

    const toggle = () =>{
        setIsOpen(!isOpen)
    };

    return (
        <>
            <Sidebar isOpen={isOpen} toggle={toggle}/>
            <Navbar toggle={toggle}/>
            <HomeSection />
            <InfoSection {...homeobjOne} />
            <InfoSection {...homeobjTwo} />
            <SignUp />
            <WeatherSection {...weatherobjOne} />
            <SignIn />
            <NavigationSection {...navigationobjOne} />
            <Location />
        </>
    )
}

export default Home