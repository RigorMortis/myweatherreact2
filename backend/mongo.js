//TÄMÄ ON MONGODB TESTITIEDOSTO:

const MongoClient = require('mongodb').MongoClient;
const uri = "mongodb+srv://myweather:REGOrego@cluster0.hv956.mongodb.net/myFirstDatabase?retryWrites=true&w=majority";

const location = {
    username: "Salomo",
    location1: "Siili 3",
    location2: "Kuha 5"
}

const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });

client.connect(err => {
    const collection = client.db("mycity").collection("locations");

    //get one
    collection.findOne({location1:"Siili 3"}).then(function(result){
       console.log(result);
    });

    //get all
    collection.find({}).toArray(function(err, locations){
        //rajaa mitä tulostat
        locations.forEach(location => console.log("nimi: " + location.username + " l1: " + location.location1));
        //tai tulosta kaikki:
        console.log(locations);
    });

    // post one
    collection.insertOne({timestamp: new Date(), username: location.username, location1:location.location1, location2:location.location2}, ((err,result) => {
        console.log("valmis");
        client.close();
    }));
});