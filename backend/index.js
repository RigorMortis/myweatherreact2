const express = require('express')
const app = express()

//Sallitaan muista origineista tulevat pyynnöt käyttämällä noden cors-middlewarea
const cors = require('cors')
app.use(cors())
//JSON-parseri (päästään käsiksi pyynnön mukana tulevaan dataan helposti)
app.use(express.json())

const MongoClient = require('mongodb').MongoClient;
const uri = "mongodb+srv://myweather:REGOrego@cluster0.hv956.mongodb.net/myFirstDatabase?retryWrites=true&w=majority";
const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });

let locations = [];

client.connect(err => {
    const collection = client.db("mycity").collection("locations");
    //get all
    collection.find({}).toArray(function(err, locationsMongo){
        locations = [...locationsMongo];
        client.close();
    });
});

//Route Home
app.get('/', (req, res) => {
    res.send('<h1>Hello!</h1>')
})

//Route get locations
app.get('/api/locations', (req, res) => {
    res.json(locations)
})

//Route get one location
app.get('/api/locations/:id', (req, res) => {
    const id = Number(req.params.id)
    const location = locations.find(location => location.id === id)
    console.log(location)
    res.json(location)
})

//Route delete location
app.delete('/api/locations/:id', (req, res) => {
    const id = Number(req.params.id)
    locations = locations.filter(location => location.id !== id)
    res.status(204).end()
})

//Generate id
const generateId = () => {
    const maxId = locations.length > 0
        ? Math.max(...locations.map(n => n.id))
        : 0
    return maxId + 1
}

//Route post location
app.post('/api/locations', (req, res) => {
    const body = req.body
    console.log(body.location1)
    if (!body.location1 || !body.location2) {
        return res.status(400).json({
            error: 'content missing'
        })
    }
    const location = {
        id: generateId(),
        username: body.username,
        location1: body.location1,
        location2: body.location2
    }

    locations = locations.concat(location)

    res.json(location)
})

const PORT = 3001
app.listen(PORT,() => {
    console.log(`Server running on port ${PORT}`)
})
